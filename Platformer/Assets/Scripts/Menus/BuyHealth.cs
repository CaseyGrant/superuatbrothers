﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BuyHealth : MonoBehaviour
{
    private Button buyHealthButton; // allows you to call the button

    void Start()
    {
        buyHealthButton = GetComponent<Button>(); // gets the button
        buyHealthButton.onClick.AddListener(TaskOnClick); // allows you to click the button
    }

    void Update()
    {
        if (GameManager.instance.playerGems < 10) // checks if the player has enough gems
        {
            buyHealthButton.interactable = false; // stops the button from being clicked
        }
        else
        {
            buyHealthButton.interactable = true; // allows the button to be clicked
        }
    }

    void TaskOnClick()
    {
        if (GameManager.instance.playerGems >= 10) // checks if the player has enough gems
        {
            GameManager.instance.playerHealth += 1; // increases the players health
            GameManager.instance.playerGems -= 10; // removes the cost of gems
        }
    }
}
