﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class PlayButton : MonoBehaviour
{
    private Button playButton; // allows you to call the button

    void Start()
    {
        playButton = GetComponent<Button>(); // gets the button
        playButton.onClick.AddListener(TaskOnClick); // allows you to click the button
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene("Level 1"); // loads level 1
    }
}
