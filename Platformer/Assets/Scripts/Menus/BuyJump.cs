﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BuyJump : MonoBehaviour
{
    private Button buyJumpButton; // allows you to call the button

    void Start()
    {
        buyJumpButton = GetComponent<Button>(); // gets the button
        buyJumpButton.onClick.AddListener(TaskOnClick); // allows you to click the button
    }

    void Update()
    {
        if (GameManager.instance.playerGems < 25) // checks if the player has enough gems
        {
            buyJumpButton.interactable = false; // stops the button from being clicked
        }
        else
        {
            buyJumpButton.interactable = true; // allows the button to be clicked
        }
    }

    void TaskOnClick()
    {
        if (GameManager.instance.playerGems >= 25) // checks if the player has enough gems
        {
            GameManager.instance.jumpNum += 1; // increases the number of jumps the player has
            GameManager.instance.playerGems -= 25; // removes the cost of gems
        }
    }
}
