﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Quit : MonoBehaviour
{
    private Button quitButton; // allows you to call the button

    void Start()
    {
        quitButton = GetComponent<Button>(); // gets the button
        quitButton.onClick.AddListener(TaskOnClick); // allows you to click the button
    }

    void TaskOnClick()
    {
        Application.Quit(); // quits the application
    }
}
