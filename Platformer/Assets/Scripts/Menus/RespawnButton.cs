﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RespawnButton : MonoBehaviour
{
    private Button respawnButton; // allows you to call the button
    public GameObject Player; // grabs the player

    void Start()
    {
        respawnButton = GetComponent<Button>(); // gets the button
        respawnButton.onClick.AddListener(TaskOnClick); // allows you to click the button
    }

    void TaskOnClick()
    {
        GameManager.instance.dead = false; // sets the dead boolean to false
        Instantiate(Player, GameManager.instance.spawnPoint, Quaternion.identity); // respawns the player
    }
}
