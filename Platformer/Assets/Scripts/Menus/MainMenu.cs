﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    private Button mainMenuButton; // allows you to call the button

    void Start()
    {
        mainMenuButton = GetComponent<Button>(); // gets the button
        mainMenuButton.onClick.AddListener(TaskOnClick); // allows you to click the button
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene("Menu"); // loads the main menu
    }
}
