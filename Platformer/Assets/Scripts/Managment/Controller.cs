﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public bool jumping; // a jump bool

    public void Move(float speed, Transform tf, Rigidbody2D rb, SpriteRenderer spr, float xMovement)
    {
        rb.velocity = new Vector2(xMovement + rb.velocity.x, rb.velocity.y); // makes the pawn move
        spr.flipX = rb.velocity.x < 0; // flips the players sprite
    }

    public void jump(Rigidbody2D rb, float jumpForce)
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce); // lifts the pawn up
        jumping = true; // sets jumping to true
    }
}
