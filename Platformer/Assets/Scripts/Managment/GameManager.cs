﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null; // sets up the game manager
    public AudioSource sourceAudio; // allows you to call the audio source
    public bool dead = false; // a dead bool
    public Vector3 spawnPoint = new Vector3(-16.93f, 11.5f, 0); // stores where the player spawns

    //////////Shop//////////
    public Camera shopCam; // sets the shops camera
    public Button respawn; // sets the respawn button
    public Text gemAmount; // sets the gem amount
    public Text healthAmount; // sets the health amount
    public Canvas shop; // sets the canvas
    public Image jumpIcon; // sets the jump icon
    public Image healthIcon; // sets the health icon
    public int playerHealth; // sets the player health
    public int playerGems; // sets the player gems
    public int jumpNum; // sets the jump amount
    ////////////////////////
    

    void Awake() // ensures that there is always a gamemanager and that there is only ever one
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        sourceAudio = GetComponent<AudioSource>(); // gets the audio source
    }

    void Update()
    {
        gemAmount.text = playerGems.ToString(); // updates the amount of gems
        healthAmount.text = playerHealth.ToString(); // updates the health amount
        if (dead)
        {
            ////////// Enables the shop /////////
            respawn.interactable = true;
            jumpIcon.enabled = true;
            healthIcon.enabled = true;
            shop.enabled = true;
            shopCam.enabled = true;
            //////////////////////////////////////
        }
        else
        {
            ////////// Disables the shop /////////
            respawn.interactable = false;
            jumpIcon.enabled = false;
            healthIcon.enabled = false;
            shop.enabled = false;
            shopCam.enabled = false;
            //////////////////////////////////////
        }

        if (playerHealth <= 0)
        {
            SceneManager.LoadScene("Game Over"); // loads the game over scene
            Destroy(gameObject); // destroys itself
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
