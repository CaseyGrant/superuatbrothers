﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCrank : MonoBehaviour
{
    public GameObject block; // sets the game object
    public Sprite crankPulled; // sets the sprite
    private SpriteRenderer spr; // sets the sprite renderer

    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            spr.sprite = crankPulled; // updates the sprite
            Destroy(block); // destroys the game object
        }
    }
}
