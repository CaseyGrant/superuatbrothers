﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEnd : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Scene currentScene = SceneManager.GetActiveScene(); // gets the current scene
            if (currentScene.name == "Level 1")
            {
                SceneManager.LoadScene("Level 2"); // gos to next level
                GameManager.instance.spawnPoint = new Vector3(-23.161f, 9.708f, 0); // updates the spawn point
            }
            if (currentScene.name == "Level 2")
            {
                SceneManager.LoadScene("Level 3"); // gos to next level
                GameManager.instance.spawnPoint = new Vector3(-13.07f, -37.77f, 0); // updates the spawn point
            }
            if (currentScene.name == "Level 3")
            {
                SceneManager.LoadScene("Win"); // gos to win scene
                Destroy(GameManager.instance); // destroys the game manager
            }
            GameManager.instance.dead = false; // sets dead to false
        }
    }
}
