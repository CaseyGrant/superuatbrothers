﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Transform tf; // sets the transform

    void Start()
    {
        tf = GetComponent<Transform>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.instance.spawnPoint = tf.position; // updates the spawn location
        }
    }
}
