﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour
{
    public AudioClip gemSound; // sets the clip
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.instance.playerGems += 1; // increases the number of gems
            GameManager.instance.gemAmount.text = GameManager.instance.playerGems.ToString(); // updates the number of gems you have
            GameManager.instance.sourceAudio.clip = gemSound; // sets the clip to the right sound
            GameManager.instance.sourceAudio.Play(); // plays a sound
            Destroy(gameObject); // destroys the gem
        }
    }
}
