﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cherry : MonoBehaviour
{
    public AudioClip cherrySound; // sets the audio clip

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.instance.playerHealth += 1; // increases the players health
            GameManager.instance.healthAmount.text = GameManager.instance.playerHealth.ToString(); // updates the players health
            GameManager.instance.sourceAudio.clip = cherrySound; // sets the clip to the right sound
            GameManager.instance.sourceAudio.Play(); // plays the clip
            Destroy(gameObject); // destroys the cherry
        }
    }
}
