﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Player : Controller
{
    public float speed; // sets the speed
    public float jumpforce; // sets the jump force
    private Transform tf; // sets the transform
    private Rigidbody2D rb; // sets the rigid body
    private SpriteRenderer spr; // sets the sprite renderer
    public Animator anim; // sets the animator
    private AudioSource sourceAudio; // sets the source audio
    public AudioClip jumpClip; // sets the jump sound
    private int tempJump; // sets a temporary jump number

    void Start()
    {
        tf = GetComponent<Transform>(); // gets the transform
        rb = GetComponent<Rigidbody2D>(); // gets the rigid body
        spr = GetComponent<SpriteRenderer>(); // gets the sprite renderer
        sourceAudio = GetComponent<AudioSource>(); // gets the source audio
        tempJump = GameManager.instance.jumpNum; // updates the number of jumps
    }

    void Update()
    {
        float xMovement = Input.GetAxisRaw("Horizontal") * speed; // sets direction and speed

        anim.SetFloat("Speed", Mathf.Abs(xMovement)); // updates the animators speed

        if (Input.GetAxis("Horizontal") < 0)
        {
            Move(speed, tf, rb, spr, xMovement); // makes the player move
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            Move(speed, tf, rb, spr, xMovement); // makes the player move
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (jumping == false || tempJump > 0)
            {
                tempJump -= 1; // decreases the current
                sourceAudio.clip = jumpClip; // sets the jump sound
                sourceAudio.Play(); // plays the jump sound
                jump(rb, jumpforce); // jumps
                anim.SetBool("IsJumping", true); //updates the animators jump
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
        {
            jumping = false; // sets jump to false
            tempJump = GameManager.instance.jumpNum; // resets the number of jumps
            anim.SetBool("IsJumping", false); // sets the animators jump to false
        }
        if (other.CompareTag("Ladder"))
        {
            Scene currentScene = SceneManager.GetActiveScene(); // gets the current scene
            if (currentScene.name == "Level 1")
            {
                jumpforce = 250; // sets the jump force
            }

            if (currentScene.name == "Level 2")
            {
                jumpforce = 100; // sets the jump force
                jumping = false; // sets jump to false
                tempJump = GameManager.instance.jumpNum; // resets the number of jumps
                anim.SetBool("IsJumping", false); // sets the animators jump to false
            }
            if (currentScene.name == "Level 3")
            {
                jumpforce = 190; // sets the jump force
            }
        }
        if (other.CompareTag("Enemy"))
        {
            GameManager.instance.playerHealth -= 1; // decreases health by 1
            GameManager.instance.healthAmount.text = GameManager.instance.playerHealth.ToString(); // update health text
            GameManager.instance.dead = true; // sets dead to true
            Destroy(gameObject); // destroys its self
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
        {
            sourceAudio.clip = jumpClip; // sets the adio clip
            sourceAudio.Play(); // plays the audio
            anim.SetBool("IsJumping", true); // sets the animators jump to true
        }
        if (other.CompareTag("Ladder"))
        {
            jumpforce = 50; // resets the jump force
        }
    }
}
