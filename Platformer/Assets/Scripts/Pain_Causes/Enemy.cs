﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Controller
{
    public float speed; // sets the speed
    private Transform tf; // sets the transform
    private Rigidbody2D rb; // sets the rigid body
    private SpriteRenderer spr; // sets the sprite renderer

    void Start()
    {
        tf = GetComponent<Transform>(); // grabs the transform
        rb = GetComponent<Rigidbody2D>(); // grabs the rigid body
        spr = GetComponent<SpriteRenderer>(); // grabs the sprite renderer
    }

    void Update()
    {
        Move(speed, tf, rb, spr, speed); // allows the enemies to move
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bounds"))
        {
            speed = -speed; // makes the enemy move the other direction
        }
    }
}
